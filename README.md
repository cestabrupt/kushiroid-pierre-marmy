# ~/ABRÜPT/PIERRE MARMY/KUSHIROID/*

La [page de ce livre](https://abrupt.ch/pierre-marmy/kushiroid/) sur le réseau.

## Sur le livre

*Kushiroid* raconte l’histoire d’une ville en ruine.

À l’est de l’île de Hokkaido, la cité de Kushiro. Une ville portuaire autrefois florissante, aujourd’hui laminée par l’époque. Démographie en berne et économie vacillante, quelques monuments témoins d’une débâcle, un centre-ville criblé de parcelles laissées à l’abandon. La désillusion règne. Kushiro est un symptôme d’une modernité qui délaisse l’urbain qui ne sert plus sa cause.

Pierre Marmy tente de saisir une urbanité qui tangue le long de son histoire. La photographie y est l’outil qui ausculte un malaise, une ville qui ne sait plus sa place dans le basculement du temps.

## Sur l'auteur

Pierre Marmy. 10.01.1993, 1000 Lausanne 26, Europe. 2019, quelque part au 4e niveau au-dessus du sol, Kreis 10, 8037 Zürich, Europe. 368 jours de service civil à errer en Suisse avec 1 appareil photographique entre les mains, parfois 2 appareils, 2190 jours d’études architecturales, 3 chats, 2 en céramique, et la photographie tout entière comme médium pour extirper la beauté du réel.

Il est notamment possible de le suivre sur son compte [instagram](https://www.instagram.com/pierre_marmy/).

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
