// General script
// Copy Data URI images to href
window.onload = function photographies() {
  var copyto = document.querySelectorAll('.galerie__lien');
  var copy = document.querySelectorAll('.galerie__image');
  for (var i = 0; i < copyto.length; i++) {
    var url = copy[i].getAttribute("src");
    copyto[i].href = url;
  }
}

// Muuri options
// Inspiration source : haltu.github.io/muuri & codepen.io/niklasramo/pen/xWYdmp
var grid = new Muuri('.grid', {
  dragEnabled: true,
  layoutOnResize: true,
  layout: {
    fillGaps: true,
  },
  dragStartPredicate: {
    distance: 10,
    // delay: 100
    // handle: '.foo, .bar'
  },
  sortData: {
    id: function (item, element) {
      return parseFloat(element.children[0].textContent);
    }
  }
});

// When all items have loaded refresh their
// dimensions and layout the grid.
window.addEventListener('load', function () {
  grid.refreshItems().layout();
  // For a little finishing touch, let's fade in
  // the images after all them have loaded and
  // they are corrertly positioned.
  document.body.classList.add('images-loaded');
});

grid.on('dragReleaseStart', function () {
  $('.item').click(false);
});

grid.on('dragReleaseEnd', function () {
  $(".item").unbind('click').click();
});

// Hold our selected filters
let filteredCategories = [];

// Get All Filterable values
const categoryFilter = document.getElementById('category-filter');
const allCategories = Array.from(categoryFilter.querySelectorAll('option'));

// Set Defaults
let selectedCategory = categoryFilter.value;

// filter categories on select
const selects = document.querySelectorAll('select');
selects.forEach( item => {
  item.addEventListener('change', () => {
    filterGrid();
  });
});

// Filter grid
function filterGrid() {
  // Get latest select values
  selectedCategory = categoryFilter.value;
  // Reset filtered categories & types
  filteredCategories.splice(0,filteredCategories.length);
  // Categories
  if( selectedCategory == 'all' ) {
    allCategories.forEach( (item) => {
      // exlude the actual 'all' value
      ( item.value !="all" ? filteredCategories.push(item.value) : '' );
    });
  } else {
    filteredCategories.push(categoryFilter.value);
  }
  console.table(filteredCategories);

  // Filter the grid
  // For each item in the grid array (which corresponds to a gallery item), check if the data-categories and data-types value match any of the values in the select field. If they do, return true
  grid.filter( (item) => {
    if (
  	  filteredCategories.some( (cat) => {
  		  return (item.getElement().dataset.category).indexOf(cat) >= 0;
  		})
  	){
  		// return items that match both IFs
  		return true;
  	}
  });
} // end filter grid function
