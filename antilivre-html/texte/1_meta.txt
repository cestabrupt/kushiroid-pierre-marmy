---
author: "Pierre Marmy"
title: "Kushiroid"
publisher: "Abrüpt"
date: "février 2019"
description: ""
subject: ""
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0036-4'
rights: "© 2019 Abrüpt, CC BY-NC-SA"
licence: 'Cet ouvrage est mis à disposition selon les termes de la Licence Creative Commons Attribution --- Pas d''Utilisation Commerciale --- Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).'
lien-livre: "https://abrupt.ch/pierre-marmy/kushiroid"
---

<div class="introduction">

<input type="checkbox" id="tab" name="tabs">
<label for="tab" class="poeme__label"><div class="revelation">pastel linceul</div><div class="cross"></div></label>
<label for="tab" class="poeme">
<p class="francais">Pastel linceul.<br>La ville est morte de ses angles. Couleurs intraveineuses tracent courbes, routes, elles vont à l’envers de l’île, que reste-t-il de l’homme si dans leur carence les couleurs vont encore à rebours de l’existence ? Elles étendent le suaire, le teintent d’ocre, d’ordre, de lavis. L’explosion, une, son écho, le siècle, le dernier se tord en écho du suivant, il attache à la mémoire et les fantômes, et les arbres, et les ruines qui flottent sous le bitume. Quelques silhouettes sans estomac, encore et seules, elles demeurent au désespoir de ne pouvoir résister, de fuir ou de se cloîtrer, d’être voraces de plastique de sorte que des masques croissent sur leur malaise. L’homme se digère, la chaux le digère, il s’évanouit sans choix, des esprits remontent les âges pour des larmes et des perles, pour que les larmes et les perles s’offrent et lavent les plaies. Imperméable la commune qui ne les retient, quelle évanescence ne suinte. Les esprits n’arrivent à effacer et l’angle et le droit, l’orthogonal est à son triomphe, il réverbère de néant. Le bâtiment se clôt, il vomit sa devanture au-dedans, se perce de fenêtres, leurs soupirs en cicatrices. Voilà la cité qui erre sans cris, l’abatteur de céans ne s’entend, il architecture ses aspirations d’autorité. Une réaction sans harmonie, l’occident tue, délaisse la dépouille, s’en retourne. Une sorcière qui danse, des démons qui attirent la vie, lui proposent le secret. Mais moderne elle se détourne. Une saignée lâche sa goutte. D’onde ou d’ombre, la nécrose ne tache le tissu, elle nuage une opalescence l’espace d’un instant. Et l’absence. Des spectres s’écorchent quelque peu aux échafauds, ils s’oxydent et rient, ils rient ou pleurent. Ils sont les rues et les morts, et les détonations qui résonnent sourdes. Un peu de grisaille pour ne pas. Ne pas voir l’inanimé, son âme violée. Cet occident qui contamine, s’en va, qui pèse toujours de son empire. Les signes se brouillent. La misère sous le linceul, le pastel, l’averse. Les signes essaient, s’échouent sur les murs, ils étendent des voiles pour ne pas. Ne pas contempler la blessure ou les canalisations qui l’évacuent. Les artères qui changent, s’en reviennent, les mêmes, de roses, de sangs et de bleus. Sans issue il y a une porte. Il y a le vide et ses créatures, elles ont lavé les ruines, elles ont liquidé la vie, ou était-ce son atome, la chaussée s’ennuie, déserte, des ruelles liquidées à perte de vue, elles ont le visage d’un homme sans rêves. Perpendiculaire il pend d’électricité au-dessus des eaux, contre-plaque l’immonde, un grain de rouille sous sa peau ou sous la peau de ville, s’éteignent les mannes de lumière et le silence remonte la ligne ennuyée, il souffle les ciels, le foyer béton en croix d’urbain.</p>

<p class="anglais">Pastel shroud.<br>The city succumbs to its angles. Intravenous colors trace curves,roads, finding their way through the island, what remains of Man if in their deficiency these colors go against the course of existence? They shroud, tint the shroud with ocher, order, and washi. The explosion, singular, its echo, that of a century, the last one twists anticipating the next, it clings to memory and ghosts and trees and ruins that float under the bitumen. Some silhouettes without stomach, still and lone, remain in despair of not being able to resist, to flee or to gather, to be voracious of plastic so that masks grow from their unease. Man digests himself, whitewash digests Man, he has no choice but to fall unconscious, spirits search the ages for tears and pearls to serve as offerings and to wash the wounds. Hermetic is the town which chooses not to retain them, what evanescence doesn’t seep. The spirits cannot erase neither the angle nor the right, the orthogonal is triumphant, and reflects a void. the building seals itself, spewing its fronts back inside, riddled with windows, their sighs into scars. Here is the city that wanders without cries, the feller of this house unheard, he structures his aspirations of authority. A reaction without harmony, the West kills, drops the corpse, has its way. A dancing witch, demons luring life, offering it their secret. Modern she turns away. A bleeding drops its drip. Of wave or shadow, the necrosis doesn’t stain the fabric, it clouds a moment of opalescence. And then the absence. Spectra brush past the scaffoldings, they oxidize and laugh, they laugh or cry. They are the streets and the dead, and the muffled detonations. A little greyness to not. To not see the inanimate, its ravaged soul. A West that contaminates, then leaves, forever carrying the weight of its empire. The signs are blurred. Misery under the shroud, the pastel, the rainfall. Signs try, wash ashore the walls, they spread their sails to not. To not contemplate the injury or the pipes that drain it. The arteries that change, return, the same, from pinks, from bloods and blues. At a dead end one finds a door. There is emptiness and its creatures, they have washed the ruins, liquidated life, or was it its atom, the roadway is idle, barren, liquidated alleys as far as the eye can see, their face that of a man who doesn’t dream. Perpendicular he hangs off electricity over waters, plies woods against the filthy, a grain of rust under his skin or under the skin of the city, are extinguished hampers of light and the silence flows along the sorry line, it blows the skies, urban cross across the concrete hearth.</p>

<p class="signature">Ann Persson.</p>
</label>

</div>

<div class="livre">

<div class="actions">
<label class="gallery-filters__filter" for="category-filter">
<span>
</span>
<select name="category-filter" id="category-filter">
<option value="all">α</option>
<option value="psi">ψ</option>
<option value="phi">ϕ</option>
<option value="mu">μ</option>
<option value="rho">ρ</option>
<option value="beta">β</option>
<option value="omega">ω</option>
<option value="theta">θ</option>
</select>
</label>
</div>

<div class="grid">
<div class="item" style="width: 30%;" data-category="psi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/01.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/02.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/03.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/04.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/05.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 60%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/06.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 20%;" data-category="psi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/07.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/08.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/09.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/10.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/11.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/12.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 20%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/13.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/14.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="psi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/15.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="psi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/16.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/17.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/18.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/19.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/20.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/21.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/22.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 20%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/23.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/24.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/25.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/26.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/27.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/28.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/29.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/30.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/31.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/32.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/33.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 60%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/34.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/35.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/36.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="beta"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/37.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="beta"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/38.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/39.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/40.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 50%;" data-category="psi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/41.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/42.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="phi"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/43.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/44.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 20%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/45.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 20%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/46.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/47.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 40%;" data-category="mu"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/48.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 20%;" data-category="omega"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/49.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="theta"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/50.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 60%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/51.jpg" class="galerie__image" />
</a></div></div>
<div class="item" style="width: 30%;" data-category="rho"><div class="item-content"><a data-fancybox="gallery" href="#" class="galerie__lien">
<img src="gabarit/img/photographies/52.jpg" class="galerie__image" />
</a></div></div>
</div>

</div>
